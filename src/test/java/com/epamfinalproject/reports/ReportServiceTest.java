package com.epamfinalproject.reports;

import com.epamfinalproject.reports.model.ReportDTO;
import com.epamfinalproject.reports.model.TrainerDocument;
import com.epamfinalproject.reports.repository.ReportRepository;
import com.epamfinalproject.reports.services.ReportService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.NoSuchElementException;
import java.util.Optional;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

@SpringBootTest
@ActiveProfiles("test")
public class ReportServiceTest {

    @Mock
    private ReportRepository reportRepository;
    @InjectMocks
    private ReportService reportService;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    public void createTraining_Successfully() {
        ReportDTO trainer = new ReportDTO();
        trainer.setTrainingDate(LocalDate.now());
        TrainerDocument trainerDocument = TrainerDocument.builder()
                .username("username")
                .trainerFirstName("John")
                .trainerLastName("Doe")
                .isActive(true)
                .summaryTrainings(new ArrayList<>())
                .build();

        when(reportRepository.findByUsername(anyString())).thenReturn(Optional.of(trainerDocument));
        when(reportRepository.save(any())).thenAnswer(i -> i.getArguments()[0]);

        assertDoesNotThrow(() -> reportService.createTraining(trainer));
    }


}
