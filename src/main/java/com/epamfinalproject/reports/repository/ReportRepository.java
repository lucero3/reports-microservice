package com.epamfinalproject.reports.repository;

import com.epamfinalproject.reports.model.TrainerDocument;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ReportRepository extends MongoRepository<TrainerDocument, String> {

    Optional<TrainerDocument> findByUsername(String username);
    List<TrainerDocument> findByTrainerFirstNameAndTrainerLastName(String trainerFirstName, String trainerLastName);

}
