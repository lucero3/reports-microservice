package com.epamfinalproject.reports.services;

import com.epamfinalproject.reports.model.ReportDTO;
import com.epamfinalproject.reports.model.SummaryMonth;
import com.epamfinalproject.reports.model.SummaryTraining;
import com.epamfinalproject.reports.model.TrainerDocument;
import com.epamfinalproject.reports.repository.ReportRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
@Slf4j
@RequiredArgsConstructor
public class ReportService {
    public final ReportRepository reportRepository;

    public void createTraining(ReportDTO trainer){
        try {
            Long transactionId = trainer.getId();
            log.info("Starting transaction: {}", transactionId);
            Optional<TrainerDocument> optionalTrainerDocument =  reportRepository.findByUsername(trainer.getTrainerUsername());

            if(optionalTrainerDocument.isPresent()) {
                TrainerDocument trainerDocument = optionalTrainerDocument.get();

                List <SummaryTraining> summaryTrainings = trainerDocument.getSummaryTrainings();

               Optional<SummaryTraining> summaryTrainingOptional =  optionalTrainerDocument.get().getSummaryTrainings().stream()
                        .filter(x -> x.getYear() == trainer.getTrainingDate().getYear())
                        .findFirst();
               if (summaryTrainingOptional.isPresent()){
                   summaryTrainingOptional.get()
                           .addDurationForMonth(trainer.getTrainingDate().getMonthValue(), trainer.getTrainingDuration());
               } else {
                   List<SummaryMonth> summaryMonths = List.of(new SummaryMonth(trainer.getTrainingDate().getMonthValue(), trainer.getTrainingDuration()));
                   SummaryTraining summaryTraining = new SummaryTraining(trainer.getTrainingDate().getYear(), summaryMonths );
                   summaryTrainings.add(summaryTraining);
               }
                trainerDocument.setSummaryTrainings(summaryTrainings);
                reportRepository.save(trainerDocument);
                log.info("Transaction: {} succeeded!", transactionId);
                return;
            }
            SummaryMonth summaryMonth = new SummaryMonth(trainer.getTrainingDate().getMonthValue(), trainer.getTrainingDuration());
            List<SummaryTraining> summaryTrainings = List.of(new SummaryTraining(trainer.getTrainingDate().getYear(), List.of(summaryMonth)));
            TrainerDocument trainerDocument = TrainerDocument.builder()
                    .username(trainer.getTrainerUsername())
                    .trainerFirstName(trainer.getTrainerFirstName())
                    .trainerLastName(trainer.getTrainerLastName())
                    .isActive(trainer.isActive())
                    .summaryTrainings(summaryTrainings)
                    .build();
            reportRepository.insert(trainerDocument);
            log.info("Transaction: {} succeeded!", transactionId);
        } catch (Exception e) {
            log.error("Transaction: {} failed with error: {}", trainer.getId(), e.getMessage());
        }
    }

}
