package com.epamfinalproject.reports.model;

import lombok.Builder;
import lombok.Getter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.CompoundIndex;
import org.springframework.data.mongodb.core.index.CompoundIndexes;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

@Getter
@Builder
@Document(collection="reports")
@CompoundIndexes({
        @CompoundIndex(name = "name_index", def = "{'trainerFirstName': 1, 'trainerLastName': 1}")
})
public class TrainerDocument {

    @Id
    String username;
    String trainerFirstName;
    String trainerLastName;
    boolean isActive;
    List<SummaryTraining> summaryTrainings;

    public void setSummaryTrainings(List<SummaryTraining> summaryTrainings) {
        this.summaryTrainings = summaryTrainings;
    }
}
