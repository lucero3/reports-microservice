package com.epamfinalproject.reports.model;

import lombok.Builder;
import lombok.Getter;

import java.util.List;
import java.util.Optional;

@Getter
@Builder
public class SummaryTraining {
    private int year;
    private List<SummaryMonth> summaryMonth;

    public SummaryTraining(int year, List<SummaryMonth> summaryMonth) {
        this.year = year;
        this.summaryMonth = summaryMonth;
    }

    public void addDurationForMonth(int month, int duration) {
            Optional<SummaryMonth> summaryMonthOptional = summaryMonth.stream()
                    .filter(monthSummary -> monthSummary.getMonth() == month)
                    .findFirst();
            if(summaryMonthOptional.isPresent()){
                int currentDuration =  summaryMonthOptional.get().getDuration();
                summaryMonthOptional.get().setDuration(currentDuration + duration);
                return;
            }
            summaryMonth.add(new SummaryMonth(month, duration));
    }
}
