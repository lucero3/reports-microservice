package com.epamfinalproject.reports.model;

import lombok.Builder;
import lombok.Getter;

@Getter
@Builder

public class SummaryMonth {
    private int month;
    private int duration;

    public SummaryMonth(int month, int duration) {
        this.month = month;
        this.duration = duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }
}
