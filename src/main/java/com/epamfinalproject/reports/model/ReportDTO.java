package com.epamfinalproject.reports.model;

import lombok.Data;

import java.time.LocalDate;
@Data
public class ReportDTO {
    private String trainerUsername;
    private String trainerFirstName;
    private String trainerLastName;
    private boolean isActive;
    private LocalDate trainingDate;
    private int trainingDuration;
    private Long id;
}
