package com.epamfinalproject.reports.queue;

import com.epamfinalproject.reports.model.ReportDTO;
import com.epamfinalproject.reports.services.ReportService;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class TrainerMSConsumer {

    private final ReportService reportService;
    private final ObjectMapper objectMapper;

    @Autowired
    public TrainerMSConsumer(ReportService reportService, ObjectMapper objectMapper) {
        this.reportService = reportService;
        this.objectMapper = objectMapper;
    }

    @JmsListener(destination = "queue.gym")
    public void receiveMessage(String messageJson) {
        log.info("Received: {}", messageJson);
        try {
            ReportDTO reportDTO = objectMapper.readValue(messageJson, ReportDTO.class);
            reportService.createTraining(reportDTO);
        } catch (Exception e) {
            log.error("Failed to deserialize or process the message", e);
            throw new RuntimeException("Error during message processing", e);
        }
    }
}
