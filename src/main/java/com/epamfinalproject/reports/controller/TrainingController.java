package com.epamfinalproject.reports.controller;

import com.epamfinalproject.reports.model.ReportDTO;
import com.epamfinalproject.reports.services.ReportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1/trainings")
public class TrainingController {

    private final ReportService reportService;

    @Autowired
    public TrainingController(ReportService reportService) {
        this.reportService = reportService;
    }

    @PostMapping("/training")
    public ResponseEntity<Void> addTrainingSummary(ReportDTO request){
            reportService.createTraining(request);
            return ResponseEntity.ok().build();
    }

}
